using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Context
{
    public class MemoriaContext<T>
    {
        static List<T> _Memoria;
        public List<T> Memoria {get => _Memoria;} 

        public MemoriaContext(){
            if (_Memoria == null)
                _Memoria = new List<T>();
        }        
    }
}