using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers.v1
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private MemoriaContext<Venda> _context;

        public VendaController(){
            _context = new MemoriaContext<Venda>();
        }

        [HttpGet]
        public IActionResult ListarTodos(){
            return Ok(_context.Memoria);
        }

        [HttpGet("{id}")]
        public IActionResult ListarPorIdVenda(int id){
            var venda = _context.Memoria.Find(x => x.Id == id);
            if (venda == null){
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpPut]
        public IActionResult AtualizardVenda(StructAtualizaVenda atualizaVenda){
            var vendaIdx = _context.Memoria.FindIndex(x => x.Id == atualizaVenda.Id);

            if (vendaIdx == -1){
                return NotFound();
            }
            var venda = _context.Memoria[vendaIdx];
            
            if (ValidaStatus.EhProximoStatusValido(venda.Status,atualizaVenda.Status))
                venda.Status = atualizaVenda.Status;
            else
                return BadRequest(new { Erro = $"A venda com Status {venda.Status} não pode seguir para o Status {atualizaVenda.Status}"});
            
            return Ok(venda);
        }

        [HttpPost]
        public IActionResult Criar(Venda venda){
            if (venda.Itens.Count == 0)
                return BadRequest(new { Erro = "A inclusão de uma venda deve possuir pelo menos 1 item"});

             if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A inclusão de uma venda deve possuir data valida"});

            if (_context.Memoria.FindIndex(x => x.Id == venda.Id) != -1)
                return BadRequest(new { Erro = $"A venda de Id {venda.Id} já existe"});

            if (!ValidaVendedor.EhVendedorValido(venda.Vendedor))
                return BadRequest(new { Erro = "Um ou mais dados do vendedor não são validos"});

            _context.Memoria.Add(venda);
            venda.Status = EnumStatus.Aguardando_pagamento;
            return CreatedAtAction(nameof(ListarPorIdVenda), new {id = venda.Id}, venda);
        }   
    }
}