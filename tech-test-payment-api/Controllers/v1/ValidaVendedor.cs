using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    public static class ValidaVendedor
    {
        public static bool EhVendedorValido(Vendedor vendedor){
            if (vendedor.Nome.Length == 0 
                || vendedor.Email.Length == 0 
                || vendedor.Cpf.Length == 0 
                || vendedor.Telefone.Length == 0)
                return false;
            
            return true;
        }
    }
}