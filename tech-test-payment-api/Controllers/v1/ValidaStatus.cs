using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    public static class ValidaStatus
    {
        private static EnumStatus[] ProximoAguardandoPagamento = {EnumStatus.Cancelada, EnumStatus.Pagamento_aprovado};
        private static EnumStatus[] ProximoPagamentoAprovado = {EnumStatus.Cancelada, EnumStatus.Enviado_para_transportadora};
        private static EnumStatus[] ProximoEnviarParaTransportadora = {EnumStatus.Entregue};
        
        public static bool EhProximoStatusValido(EnumStatus statusAtual, EnumStatus proximoStatus){
            switch (statusAtual){
                case EnumStatus.Aguardando_pagamento:
                    return ProximoAguardandoPagamento.Contains(proximoStatus);
                case EnumStatus.Pagamento_aprovado:
                    return ProximoPagamentoAprovado.Contains(proximoStatus);
                case EnumStatus.Enviado_para_transportadora:
                    return ProximoEnviarParaTransportadora.Contains(proximoStatus);
                default:
                    return false;    
            }          
        }
    }
}