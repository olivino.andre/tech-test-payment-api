using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class StructAtualizaVenda
    {
        public int Id { get; set; }
        public EnumStatus Status { get; set; }
    }
}