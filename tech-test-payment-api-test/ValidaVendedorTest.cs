using tech_test_payment_api.Models;
using tech_test_payment_api.Controllers;

namespace tech_test_payment_api_test{

    public class ValidaVendedorTest
    {    
        [Theory]
        [InlineData("","","","",false)]
        [InlineData("Maria","","","",false)]
        [InlineData("Maria","123.456.789-10","","",false)]
        [InlineData("Maria","123.456.789-10","maria@email.com","",false)]
        [InlineData("Maria","123.456.789-10","maria@email.com","(00) 91111-1111",true)]
        public void TesteCampos(string nome, string cpf, string email, string telefone, bool esperado)
        {
            Vendedor vendedor = new Vendedor();
            vendedor.Nome = nome;
            vendedor.Cpf = cpf;
            vendedor.Email = email;
            vendedor.Telefone = telefone;
            Assert.Equal(ValidaVendedor.EhVendedorValido(vendedor),esperado);
        }
    }

}

