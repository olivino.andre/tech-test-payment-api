using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;
using tech_test_payment_api.Controllers;

namespace tech_test_payment_api_test
{
    public class ValidaStatusTest
    {
        [Theory]
        [InlineData(EnumStatus.Aguardando_pagamento,EnumStatus.Cancelada,true)]
        [InlineData(EnumStatus.Aguardando_pagamento,EnumStatus.Pagamento_aprovado,true)]
        [InlineData(EnumStatus.Aguardando_pagamento,EnumStatus.Entregue,false)]
        [InlineData(EnumStatus.Aguardando_pagamento,EnumStatus.Enviado_para_transportadora,false)]
        [InlineData(EnumStatus.Pagamento_aprovado,EnumStatus.Enviado_para_transportadora,true)]
        [InlineData(EnumStatus.Pagamento_aprovado,EnumStatus.Cancelada,true)]
        [InlineData(EnumStatus.Enviado_para_transportadora,EnumStatus.Entregue,true)]
        [InlineData(EnumStatus.Enviado_para_transportadora,EnumStatus.Cancelada,false)]
        [InlineData(EnumStatus.Cancelada,EnumStatus.Aguardando_pagamento,false)]
        [InlineData(EnumStatus.Entregue,EnumStatus.Pagamento_aprovado,false)]
        
        public void TestProximoStatus(EnumStatus atual, EnumStatus proximo, bool esperado)
        {
            Assert.Equal(ValidaStatus.EhProximoStatusValido(atual,proximo),esperado);
        }
    }
}